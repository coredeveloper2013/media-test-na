<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware('auth')->group(function() {
    Route::get('/smtp_info', function () {
        return view('smtpInfo');
    })->name('smtp.info');
    Route::get('/email', function () {
        return view('mailInfo');
    })->name('email.page');

    // api route
    Route::post('/smtp_store', 'SmtpController@store')->name('smtp.info.store');
    Route::get('/smtp_info_index', 'SmtpController@index')->name('smtp.info.index');
    Route::get('/emails', 'EmailController@index')->name('email.index');
    Route::post('/email_send', 'EmailController@send')
        ->middleware('mail.config')
        ->name('email.send');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
